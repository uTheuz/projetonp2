package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class RecuperarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar);

        final Button btnRecupera = findViewById(R.id.btnRecupera);


        btnRecupera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText edtMail = findViewById(R.id.edtMail);
                String Email = edtMail.getText().toString();

                if(Email.isEmpty()){
                    edtMail.setError("Informe o e-mail cadastrado");
                    edtMail.requestFocus();
                }else{
                    Apoio apoio = new Apoio();
                    Ion.with(RecuperarActivity.this)
                            .load(apoio.getChamarServidor() + "recuperar.php")
                            .setBodyParameter("Email", Email)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    //Tratativa de erro
                                    try{
                                        //Verifica a disponibilidade do banco de dados
                                        if(result.get("Cod").getAsString().equals("0")){
                                            //Não foi possível se conectar ao banco de dados
                                            Toast.makeText(RecuperarActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                        }else if(result.get("Cod").getAsString().equals("2")){
                                            //E-mail inválido
                                            edtMail.setText("");
                                            edtMail.setError("Endereço de e-mail inválido!");
                                            edtMail.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("3")){
                                            //E-mail não cadastrado
                                            Toast.makeText(RecuperarActivity.this, "E-mail não cadastrado!", Toast.LENGTH_LONG).show();
                                            edtMail.setError("E-mail não cadastrado");
                                            edtMail.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("4")){
                                            //Não foi possível enviar o e-mail
                                            Toast.makeText(RecuperarActivity.this, "Não foi possível enviar o e-mail, tente novamente!", Toast.LENGTH_LONG).show();
                                            edtMail.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("5")){
                                            //E-mail enviado com sucesso
                                            Toast.makeText(RecuperarActivity.this, "Dados enviados via e-mail!", Toast.LENGTH_LONG).show();
                                            edtMail.requestFocus();
                                            Intent ActEntrar = new Intent(RecuperarActivity.this, EntrarActivity.class);
                                            startActivity(ActEntrar);

                                        }else{
                                            //Para valores fora do esperado
                                            Toast.makeText(RecuperarActivity.this, "Erro na requisição, tente novamente!", Toast.LENGTH_LONG).show();
                                            edtMail.requestFocus();

                                        }
                                    }catch (Exception a){

                                        Toast.makeText(RecuperarActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();
                                    }

                                }
                            });
                }
            }
        });



    }
}
