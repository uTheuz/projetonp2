package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import fate.edu.br.quizti.*;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class PerfilActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);


        //Lista os campos que precisa serem setadotos
        final TextView tvQtdAcertos = findViewById(R.id.tvQtdAcertos);
        final TextView tvPerAcertos = findViewById(R.id.tvPerAcertos);
        final TextView tvQtdErros = findViewById(R.id.tvQtdErros);
        final TextView tvPerErros = findViewById(R.id.tvPerErros);
        final TextView tvQtdReinicio = findViewById(R.id.tvQtdReinicio);
        final TextView tvTaxaSucesso = findViewById(R.id.tvTaxaSucesso);

        Apoio apoio = new Apoio();

        //Alimentar com o ID do usuário
        String ID = EntrarActivity.IdUsuario;

        Ion.with(PerfilActivity.this)
                .load(apoio.getChamarServidor() + "perfil.php")
                .setBodyParameter("ID", ID)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        //Tratativa de erro
                        try {
                            //Verifica a disponibilidade do banco de dados
                            if (result.get("Cod").getAsString().equals("0")) {
                                //Não foi possível se conectar ao banco de dados
                                Toast.makeText(PerfilActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();
                                tvQtdAcertos.setText("0");
                                tvQtdErros.setText("0");
                                tvPerAcertos.setText("0");
                                tvPerErros.setText("0");
                                tvQtdReinicio.setText("0");
                                tvTaxaSucesso.setText("0");

                            } else if (result.get("Cod").getAsString().equals("2")) {
                                //Alimentar dados com itens da API
                                tvQtdAcertos.setText(result.get("Act").getAsString());
                                tvQtdErros.setText(result.get("Ers").getAsString());
                                tvPerAcertos.setText(result.get("Pa").getAsString());
                                tvPerErros.setText(result.get("Pe").getAsString());
                                tvQtdReinicio.setText(result.get("RI").getAsString());
                                tvTaxaSucesso.setText(result.get("Pts").getAsString());

                                if(result.get("Qa").getAsString().equals(result.get("Qt").getAsString())){

                                    Button btnPerfilQuiz = findViewById(R.id.btnPerfilQuiz);
                                    btnPerfilQuiz.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Toast.makeText(PerfilActivity.this, "Todas as perguntas já respondidas, aguarde uma nova atualização!", Toast.LENGTH_LONG).show();
                                        }
                                    });

                                }else{

                                    Button btnPerfilQuiz = findViewById(R.id.btnPerfilQuiz);
                                    btnPerfilQuiz.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                            Intent ActPergunta = new Intent(PerfilActivity.this, PerguntaActivity.class);
                                            startActivity(ActPergunta);
                                        }
                                    });

                                }

                            } else {
                                //Para valores fora do esperado
                                Toast.makeText(PerfilActivity.this, "Erro na requisição, tente novamente!", Toast.LENGTH_LONG).show();
                                tvQtdAcertos.setText("0");
                                tvQtdErros.setText("0");
                                tvPerAcertos.setText("0");
                                tvPerErros.setText("0");
                                tvQtdReinicio.setText("0");
                                tvTaxaSucesso.setText("0");

                            }
                        } catch (Exception a) {
                            //Informa erro
                            Toast.makeText(PerfilActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();

                            //Seta os campos como 0
                            tvQtdAcertos.setText("0");
                            tvQtdErros.setText("0");
                            tvPerAcertos.setText("0");
                            tvPerErros.setText("0");
                            tvQtdReinicio.setText("0");
                            tvTaxaSucesso.setText("0");
                        }

                    }
                });

        Button btnPerfilSair = findViewById(R.id.btnPerfilSair);
        btnPerfilSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}
