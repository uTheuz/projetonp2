package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import fate.edu.br.quizti.*;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class PerguntaActivity extends AppCompatActivity {

    //Lista de Variáveis - Gerais
    String QuestaoInicial;
    String OpcaoResposta;
    String RespostaUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pergunta);

        String Teste;

        //Conexão com a API
        final Apoio apoio = new Apoio();

        //Solicita questão de Start
        Ion.with(PerguntaActivity.this)
                .load(apoio.getChamarServidor() + "perfil.php")
                .setBodyParameter("ID", EntrarActivity.IdUsuario)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject resultado) {
                        //Tratativa de erro
                        try{

                            //Todas as perfuntas já foram respondidas
                            if(resultado.get("Qa").getAsString().equals(resultado.get("Qt").getAsString())){
                                finish();
                                Intent ActPerfil = new Intent(PerguntaActivity.this, PerfilActivity.class);
                                startActivity(ActPerfil);
                                Toast.makeText(PerguntaActivity.this, "Todas as perguntas já respondidas, aguarde uma nova atualização!", Toast.LENGTH_LONG).show();
                            }

                            //Variáveis
                            final TextView tvPergunta = findViewById(R.id.tvPergunta);
                            final RadioGroup rgOpcao = findViewById(R.id.rgOpcao);
                            final RadioButton ItemA = findViewById(R.id.ItemA);
                            final RadioButton ItemB = findViewById(R.id.ItemB);
                            final RadioButton ItemC = findViewById(R.id.ItemC);
                            final RadioButton ItemD = findViewById(R.id.ItemD);

                            //Realiza a requisição dos dados da questão
                            Ion.with(PerguntaActivity.this)
                                    .load(apoio.getChamarServidor() + "questao.php")
                                    .setBodyParameter("IdQuestao",  resultado.get("Stq").getAsString())
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            //Tratativa de erro
                                            try{
                                                if(result.get("Cod").getAsString().equals("0")){
                                                    //Não foi possível se conectar ao banco de dados
                                                    Toast.makeText(PerguntaActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                                }else if(result.get("Cod").getAsString().equals("2")){
                                                    //Carrega os dados da questão
                                                    tvPergunta.setText(result.get("Questao").getAsString());
                                                    ItemA.setText(result.get("ItemA").getAsString());
                                                    ItemB.setText(result.get("ItemB").getAsString());
                                                    ItemC.setText(result.get("ItemC").getAsString());
                                                    ItemD.setText(result.get("ItemD").getAsString());
                                                    OpcaoResposta = result.get("ItemR").getAsString();

                                                }
                                            }catch(Exception a){
                                                Toast.makeText(PerguntaActivity.this, "Não foi possível atender sua solicitação - Consulta os dados da questao!", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                            Button btnResponder = findViewById(R.id.btnResponder);
                            //Botão Responder
                            btnResponder.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    switch (rgOpcao.getCheckedRadioButtonId()){
                                        case R.id.ItemA:
                                            RespostaUsuario = "A";
                                            break;
                                        case R.id.ItemB:
                                            RespostaUsuario = "B";
                                            break;
                                        case R.id.ItemC:
                                            RespostaUsuario = "C";
                                            break;
                                        case R.id.ItemD:
                                            RespostaUsuario = "D";
                                            break;
                                        default:
                                            Toast.makeText(PerguntaActivity.this, "Por favor, selecione um item!", Toast.LENGTH_LONG).show();
                                            break;
                                    }

                                    if(!(RespostaUsuario == null)){
                                        if(OpcaoResposta.equals(RespostaUsuario)){
                                            //Desabilita as opções
                                            for (int i = 0; i <rgOpcao.getChildCount(); i++) {
                                                rgOpcao.getChildAt(i).setEnabled(false);
                                            }

                                            //Solicita atualização de dados para a API
                                            Ion.with(PerguntaActivity.this)
                                                    .load(apoio.getChamarServidor() + "pontos.php")
                                                    .setBodyParameter("IdUser", EntrarActivity.IdUsuario)
                                                    .setBodyParameter("Acerto", "1")
                                                    .setBodyParameter("Erros", "0")
                                                    .setBodyParameter("Reinicio", "0")
                                                    .setBodyParameter("Pontos", "10")
                                                    .setBodyParameter("StartQuestao", "1")
                                                    .asJsonObject()
                                                    .setCallback(new FutureCallback<JsonObject>() {
                                                        @Override
                                                        public void onCompleted(Exception e, JsonObject result) {

                                                            //Tratativa de erro
                                                            try{
                                                                if(result.get("Cod").getAsString().equals("0")){
                                                                    //Não foi possível se conectar ao banco de dados
                                                                    Toast.makeText(PerguntaActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                                                }else if(result.get("Cod").getAsString().equals("2")){
                                                                    Intent Reiniciar = getIntent();
                                                                    finish();
                                                                    startActivity(Reiniciar);

                                                                }
                                                            }catch(Exception a){
                                                                Toast.makeText(PerguntaActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });

                                            //Informa ao usuário
                                            Toast.makeText(PerguntaActivity.this, "Acertou", Toast.LENGTH_LONG).show();

                                        }else {
                                            //Desabilita as opções
                                            for (int i = 0; i <rgOpcao.getChildCount(); i++) {
                                                rgOpcao.getChildAt(i).setEnabled(false);
                                            }

                                            //Solicita atualização de dados para a API
                                            Ion.with(PerguntaActivity.this)
                                                    .load(apoio.getChamarServidor() + "pontos.php")
                                                    .setBodyParameter("IdUser", EntrarActivity.IdUsuario)
                                                    .setBodyParameter("Acerto", "0")
                                                    .setBodyParameter("Erros", "1")
                                                    .setBodyParameter("Reinicio", "0")
                                                    .setBodyParameter("Pontos", "-10")
                                                    .setBodyParameter("StartQuestao", "1")
                                                    .asJsonObject()
                                                    .setCallback(new FutureCallback<JsonObject>() {
                                                        @Override
                                                        public void onCompleted(Exception e, JsonObject result) {

                                                            //Tratativa de erro
                                                            try{
                                                                if(result.get("Cod").getAsString().equals("0")){
                                                                    //Não foi possível se conectar ao banco de dados
                                                                    Toast.makeText(PerguntaActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                                                }else if(result.get("Cod").getAsString().equals("2")){
                                                                    Intent Reiniciar = getIntent();
                                                                    finish();
                                                                    startActivity(Reiniciar);

                                                                }
                                                            }catch(Exception a){
                                                                Toast.makeText(PerguntaActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });

                                            //Informa ao usuário
                                            Toast.makeText(PerguntaActivity.this, "Errou", Toast.LENGTH_LONG).show();
                                        }
                                    }

                                }
                            });
                        }catch(Exception a){
                            Toast.makeText(PerguntaActivity.this, "Não foi possível atender sua solicitação", Toast.LENGTH_LONG).show();
                        }
                    }
                });

        Button btnIrPerfil = findViewById(R.id.btnIrPerfil);
        btnIrPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent ActPerfil = new Intent(PerguntaActivity.this, PerfilActivity.class);
                startActivity(ActPerfil);
            }
        });

    }
}
