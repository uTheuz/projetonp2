package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class EntrarActivity extends AppCompatActivity {

    public static String IdUsuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrar);

        //Oculta o Elemento do Token
        final EditText edtToken = findViewById(R.id.edtToken);

        edtToken.setVisibility(View.INVISIBLE);

        Button btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText edtUser = findViewById(R.id.edtUser);


                String Usuario = edtUser.getText().toString();
				String Token = edtToken.getText().toString();

				if(Token.isEmpty()){
				    Token = "null";
                }

                if(Usuario.isEmpty()){
                    edtUser.setError("Informe um usuário");
                    edtUser.requestFocus();
                }else{
                    final Apoio apoio = new Apoio();
                    Ion.with(EntrarActivity.this)
                            .load(apoio.getChamarServidor() + "login.php")
                            .setBodyParameter("Verificar", Usuario)
                            .setBodyParameter("Token", Token)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {

                                    //Tratativa de erro
                                    try{

                                        if(result.get("Cod").getAsString().equals("0")){
                                            //Não foi possível se conectar ao banco de dados
                                            Toast.makeText(EntrarActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                        }else if(result.get("Cod").getAsString().equals("2")){
                                            //E-mail ou usuário não cadastrado
                                            edtUser.setText("");
                                            edtUser.setError("E-mail ou Usuário não cadastrado!");
                                            edtUser.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("3")){
                                            //Chama a activity de perfil - Liberação sem Token
                                            IdUsuario = result.get("ID").getAsString();
                                            Intent ActPerfil = new Intent(EntrarActivity.this, PerfilActivity.class);
                                            startActivity(ActPerfil);
                                        }else if(result.get("Cod").getAsString().equals("4")){
                                            //Solicita Token
                                            edtToken.setVisibility(View.VISIBLE);
                                            edtToken.setText("");
                                            edtToken.setError("Informe o Token de acesso");
                                            edtToken.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("5")){
                                            //Chama a activity de perfil - Liberação com Token
                                            IdUsuario = result.get("ID").getAsString();
                                            Intent ActPerfil = new Intent(EntrarActivity.this, PerfilActivity.class);
                                            startActivity(ActPerfil);

                                        }else{
                                            //Para valores fora do esperado
                                            Toast.makeText(EntrarActivity.this, "Erro na requisição, tente novamente!", Toast.LENGTH_LONG).show();
                                            edtUser.requestFocus();

                                        }
                                    }catch(Exception a){
                                        Toast.makeText(EntrarActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            }
        });

    }
}
