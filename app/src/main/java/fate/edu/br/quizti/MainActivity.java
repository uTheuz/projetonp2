package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*Ação: Chamar tela de cadastro*/
        Button btnEntrar = findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Chama a tela de Login
                Intent ActEntrar = new Intent(MainActivity.this, EntrarActivity.class);
                startActivity(ActEntrar);
            }
        });

        /*Ação: Chamar botão de cadastro*/
        Button btnCadastrar = findViewById(R.id.btnCadastrar);
        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Chama a tela de Cadastro
                Intent ActCadastrar = new Intent(MainActivity.this, CadastrarActivity.class);
                startActivity(ActCadastrar);
            }
        });

        /*Ação: Chamar tela de resgatar usuário*/
        Button btnRecuperar = findViewById(R.id.btnRecuperar);
        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Chama a tela de recuperação
                Intent ActRecuperar = new Intent(MainActivity.this, RecuperarActivity.class);
                startActivity(ActRecuperar);
            }
        });

        /*Ação: Sair da aplicação*/
        Button btnSair = findViewById(R.id.btnIrPerfil);
        btnSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //Informa o status do banco de dados
        final TextView tvStatusDb = findViewById(R.id.tvStatusDb);

        Apoio apoio = new Apoio();
        Ion.with(MainActivity.this)
                .load(apoio.getChamarServidor() + "status_db.php")
                .setBodyParameter("", "")
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {

                        //Tratativa de erro
                        try{
                            if(result.get("Cod").getAsString().equals("1")){
                                tvStatusDb.setText("Online");
                            }else{
                                tvStatusDb.setText("Offline");
                            }
                        }catch(Exception a){
                            tvStatusDb.setText("Offline");
                        }
                    }
                });

    }
}
