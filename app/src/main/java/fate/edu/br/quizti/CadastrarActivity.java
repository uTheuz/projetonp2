package fate.edu.br.quizti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

public class CadastrarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        //Mantem o foco no start da act
        EditText edtInput = findViewById(R.id.edtInputUser);
        edtInput.requestFocus();

        //Ao clicar no botão cadastrar
        Button btnCadastra = findViewById(R.id.btnCadastra);
        btnCadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText edtInputUser = findViewById(R.id.edtInputUser);
                final EditText edtInputMail = findViewById(R.id.edtInputMail);
                String Usuario = edtInputUser.getText().toString();
                String Email = edtInputMail.getText().toString();

                if(Usuario.isEmpty()){
                    edtInputUser.setError("Informe um valor válido!");
                    edtInputUser.requestFocus();

                }else if(Email.isEmpty()){
                    edtInputMail.setError("Informe um valor válido!");
                    edtInputMail.requestFocus();

                }else{
                    final Apoio apoio = new Apoio();
                    Ion.with(CadastrarActivity.this)
                            .load(apoio.getChamarServidor() + "usuario.php")
                            .setBodyParameter("Email", Email)
                            .setBodyParameter("Usuario", Usuario)
                            .asJsonObject()
                            .setCallback(new FutureCallback<JsonObject>() {
                                @Override
                                public void onCompleted(Exception e, JsonObject result) {
                                    //Tratativa de erro
                                    try{
                                        //Verifica se o banco está disponível
                                        if(result.get("Cod").getAsString().equals("0")){
                                            //Não foi possível se conectar ao banco de dados
                                            Toast.makeText(CadastrarActivity.this, "Erro DB01: Tente novamente mais tarde!", Toast.LENGTH_LONG).show();

                                        }else if(result.get("Cod").getAsString().equals("2")){
                                            //E-mail inválido
                                            edtInputMail.setText("");
                                            edtInputMail.setError("Endereço de e-mail inválido!");
                                            edtInputMail.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("3")){
                                            //E-mail já cadastrado
                                            Toast.makeText(CadastrarActivity.this, "E-mail já cadastrado!", Toast.LENGTH_LONG).show();
                                            edtInputMail.setError("E-mail já cadastrado");
                                            edtInputMail.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("4")){
                                            //Usuário já existe
                                            Toast.makeText(CadastrarActivity.this, "Usuário já cadastrado!", Toast.LENGTH_LONG).show();
                                            edtInputUser.setError("Usuário já cadastrado");
                                            edtInputUser.requestFocus();

                                        }else if(result.get("Cod").getAsString().equals("5")){
                                            //Erro no momento de gravar os dados
                                            Toast.makeText(CadastrarActivity.this, "Erro DB02: Erro ao realizar o cadastro!", Toast.LENGTH_LONG).show();

                                        }else if(result.get("Cod").getAsString().equals("6")){
                                            //Usuário gravado com sucesso
                                            Toast.makeText(CadastrarActivity.this, "Usuário cadastrado com sucesso!", Toast.LENGTH_LONG).show();
                                            edtInputMail.setText("");
                                            edtInputUser.setText("");
                                            edtInputUser.requestFocus();
                                            finish();
                                            Intent ActEntrar = new Intent(CadastrarActivity.this, EntrarActivity.class);
                                            startActivity(ActEntrar);

                                        }else{
                                            //Para valores fora do esperado
                                            Toast.makeText(CadastrarActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();

                                        }

                                    }catch (Exception a){
                                        Toast.makeText(CadastrarActivity.this, "Não foi possível atender sua solicitação!", Toast.LENGTH_LONG).show();
                                    }

                                }
                            });

                }

            }
        });


    }
}
